#!/bin/bash

set -x

ibmcloud cr build --tag us.icr.io/lyndell-cr/mtr:0.1   -f mtr.Dockerfile   .
ibmcloud cr build --tag us.icr.io/lyndell-cr/fping:0.1 -f fping.Dockerfile .
ibmcloud cr build --tag us.icr.io/lyndell-cr/curl:0.1  -f curl.Dockerfile  .

