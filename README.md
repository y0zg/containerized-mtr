Effective network troubleshooting requires running network test from the same source and destination as observed by the user.  By default, access to the host of a Kubernetes worker node is prohibited.  But MTR can be containerized.  

## Quick Usage

Transient usage can use the `run` command with arguments for the
`mtr` command.  

1. Rebuild the image with the `mtr.Dockerfile` file.
2. `kubectl run NAME --image=image -- [args...]`

For example:

```
ibmcloud cr build --tag us.icr.io/lyndell-cr/mtr .
kubectl run  -it --rm mtr --image us.icr.io/lyndell-cr/mtr:0.1 -- -r -c NUM DOMAIN
```

Sample output running on a cluster:

```
Lyndell@Work containerized-mtr c[_] kubectl run  -it --rm mtr --image us.icr.io/lyndell-cr/mtr:0.1 -- -r -c3 Lyndell.NET
kubectl run --generator=deployment/apps.v1 is DEPRECATED and will be removed in a future version. Use kubectl run --generator=run-pod/v1 or kubectl create instead.
If you don't see a command prompt, try pressing enter.
HOST: mtr-688bf878b5-sk499        Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.177.229.55              0.0%     3    0.2   0.1   0.1   0.2   0.0
  2.|-- 169.254.173.179            0.0%     3    2.6   1.7   1.1   2.6   0.8
  3.|-- ae104.ppr02.dal10.network  0.0%     3    4.2   2.1   1.0   4.2   1.8
  4.|-- ae3.dar02.dal10.networkla  0.0%     3    0.6   0.7   0.6   0.8   0.1
  5.|-- ae11.cbs01.dr01.dal04.net 66.7%     3    2.4   2.4   2.4   2.4   0.0
  6.|-- ae3.cbs02.sr02.hou02.netw 66.7%     3    7.5   7.5   7.5   7.5   0.0
  7.|-- ae14.bbr02.sr02.hou02.net  0.0%     3    7.1   8.1   7.0  10.4   1.9
  8.|-- ae6.dar02.sr02.hou02.netw  0.0%     3    6.7  12.6   6.6  24.4  10.2
  9.|-- po2.fcr01.sr02.hou02.netw  0.0%     3   14.9   9.9   7.1  14.9   4.3
 10.|-- server2.rpmfiles.net       0.0%     3    6.5   6.5   6.5   6.6   0.1
Session ended, resume using 'kubectl attach mtr-688bf878b5-sk499 -c mtr -i -t' command when the pod is running
deployment.apps "mtr" deleted
Lyndell@Work containerized-mtr c[_] 
```

## Deployment

Rebuild the image with the `Dockerfile` 

The ENTRYPOINT requires an IP.  See example YAML:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mtr-loop
spec:
  selector:
    matchLabels:
      app: mtr-loop
  replicas: 1
  template:
    metadata:
      labels:
        app: mtr-loop
    spec:
      containers:
      - name: mtr-loop
        image: us.icr.io/lyndell-cr/mtr-loop:0.1
        imagePullPolicy: Always
        args: ["52.117.30.130"]
```

Find `mtr-loop.yaml` in this project.

Replace the IP with your local IP.  Look up your office site's external IP at the [ifconfig.me](http://ifconfig.me/ip).

## View Results

Watch the logs from the cluster to remote IP.

```
Lyndell@Work containerized-mtr c[_] kubectl logs mtr-loop-76877b4f5f-2hv8x
Wed Feb 26 02:42:33 UTC 2020

Start: 2020-02-26T02:42:33+0000
HOST: mtr-loop-76877b4f5f-2hv8x   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.177.229.5               0.0%   100    0.1   0.1   0.1   0.8   0.1
  2.|-- 169.254.173.178            0.0%   100    0.8   1.2   0.7   5.6   1.0
  3.|-- vl2.fcr01b.dal10.networkl  0.0%   100    1.4   2.4   0.8  20.1   3.4
  4.|-- ae104.ppr01.dal10.network  0.0%   100    3.2   3.2   0.9  24.6   4.8
  5.|-- ae2.dar02.dal10.networkla  0.0%   100    0.7   1.3   0.5  18.2   2.4
  6.|-- ae11.cbs02.dr01.dal04.net 50.0%   100    2.2   3.5   2.1  18.0   3.1
  7.|-- ae3.dar02.dal13.networkla  0.0%   100    2.4   3.0   2.3  15.6   2.1
  8.|-- 8b.76.30a9.ip4.static.sl-  0.0%   100    2.6   3.9   2.4  24.5   3.5
  9.|-- d9.76.30a9.ip4.static.sl-  0.0%   100    5.1   4.8   2.3  46.4   5.2
 10.|-- 82.1e.7534.ip4.static.sl-  0.0%   100    2.1   3.3   1.9  19.2   3.5


Wed Feb 26 02:44:19 UTC 2020

Start: 2020-02-26T02:44:19+0000
HOST: mtr-loop-76877b4f5f-2hv8x   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.177.229.5               0.0%   100    0.1   0.1   0.1   0.2   0.0
  2.|-- 169.254.173.178            0.0%   100    1.3   1.4   0.8  22.5   2.4
  3.|-- vl2.fcr01b.dal10.networkl  0.0%   100    1.0   2.1   0.8  17.4   2.8
  4.|-- ae104.ppr01.dal10.network  0.0%   100   18.7   2.6   0.9  28.7   4.1
  5.|-- ae2.dar02.dal10.networkla  0.0%   100    0.6   1.2   0.4  17.2   2.4
  6.|-- ae11.cbs02.dr01.dal04.net 39.0%   100    2.8   3.4   2.1  20.4   2.8
  7.|-- ae3.dar02.dal13.networkla  0.0%   100    2.5   5.6   2.3  59.4   9.4
  8.|-- 8b.76.30a9.ip4.static.sl-  0.0%   100    2.5   3.8   2.4  26.5   3.3
  9.|-- d9.76.30a9.ip4.static.sl-  0.0%   100    7.8   4.5   2.3  27.8   3.6
 10.|-- 82.1e.7534.ip4.static.sl-  0.0%   100    2.1   2.4   2.0  16.6   1.7

```

## MTR to Cluster

To run `mtr` back to the cluster, on Mac, Linux, or UNIX-like OSes, run:

`mtr -r -c100 l2.us-south.containers.appdomain.cloud`

This can be looped for a longterm run:

`while true ; do mtr -r -c100 l2.us-south.containers.appdomain.cloud ; sleep 300; done`

Or run this MTR loop [script](https://gitlab.com/lyndell/local-mtr-loop)
