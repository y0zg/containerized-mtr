#!/bin/bash
if [ $1 ]; then
  TESTIP=${1}
else
  echo "USAGE: Enter an IP to run MTR to."
  exit 128
fi

while true
do
  date -u
  echo ""
  mtr -r -c 100 ${TESTIP}
  echo ""
  echo ""
done

